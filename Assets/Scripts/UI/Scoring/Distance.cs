using UnityEngine;
using UnityEngine.UI;

public class Distance : MonoBehaviour
{
    public Transform playersPosition;
    public Text distanceText;

    private PlayerMove playerMove;

    private int lastPosition;

    private void Start()
    {
        playerMove = GameObject.Find("Player").GetComponent<PlayerMove>();

        lastPosition = playerMove.LastPosition;
    }

    private void Update()
    {
        if (lastPosition != playerMove.LastPosition)
        {
            lastPosition = playerMove.LastPosition;
            UpdatePosition(lastPosition);
        }
    }

    private void UpdatePosition(int posZ)
    {
        distanceText.text = posZ.ToString();
    }
}
