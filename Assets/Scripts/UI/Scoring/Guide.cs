using System.Collections;
using UnityEngine;

public class Guide : MonoBehaviour
{
    public int waitSecondsBeforeHide;
    public GameObject guideUI;

    private void Awake()
    {
        waitSecondsBeforeHide = 5;
    }

    private void Start()
    {
        StartCoroutine(Hide());
    }

    public void HideGuide(bool hide)
    {
        guideUI.SetActive(!hide);
    }

    private IEnumerator Hide()
    {
        yield return new WaitForSeconds(waitSecondsBeforeHide);
        HideGuide(true);
    }
}
