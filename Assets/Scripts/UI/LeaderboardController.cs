using UnityEngine;
using LootLocker.Requests;
using System.Collections.Generic;
using System.Linq;

public class LeaderboardController : MonoBehaviour
{
    private string memberID;
    private int leaderboardID;
    private GameObject loader;

    public int score = -1;
    public int numberOfShowingScores;
    public bool autoStart = false;

    private string username;

    private void Start()
    {
        loader = GameObject.FindGameObjectWithTag("Loader");

        StartLootLockerGuestSession();
    }

    public void StartLootLockerGuestSession()
    {
        LootLockerSDKManager.StartGuestSession((response) =>
        {
            if (!response.success)
            {
                Debug.Log("Error starting LootLocker session");
                return;
            }

            memberID = response.player_id.ToString();
            leaderboardID = 3814;
            numberOfShowingScores = 5;

            if (Player.Instance?.Username != null)
            {
                username = Player.Instance.Username;
            }

            if (autoStart)
                GetLeaderBoardScores();
        });
    }

    public void SubmitScore(int score)
    {
        if (memberID.Trim() != null && score >= 0 && leaderboardID > 0)
        {
            LootLockerSDKManager.SubmitScore(memberID, score, leaderboardID, metadata: username, (response) =>
            {
                if (response.statusCode == 200)
                {
                    Debug.Log("Score submitted succesfully");
                }
                else
                {
                    Debug.Log($"Score submission failed: {response.Error}");
                }
            });
        }
    }

    public void GetLeaderBoardScores()
    {
        List<LootLockerLeaderboardMember> results = null;

        LootLockerSDKManager.GetScoreList(leaderboardID, numberOfShowingScores, (response) =>
        {
            if (response.statusCode == 200)
            {
                results = response.items.ToList();

                GameObject gridLayoutObject = GameObject.FindGameObjectWithTag("GridLayout");
                gridLayoutObject.SendMessage("PopulateData", results);
            }
            else
            {
                Debug.Log("failed: " + response.Error);
            }

            if (loader != null)
            {
                loader.SendMessage("StopLoadingEventHandler");
            }
        });
    }
    
}
