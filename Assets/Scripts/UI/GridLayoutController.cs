using UnityEngine;
using UnityEngine.UI;

public class GridLayoutController : MonoBehaviour
{
    public GameObject parent;

    public int divideCols;
    public int height;

    private void Start()
    {
        parent = gameObject.transform.parent.gameObject;
        SetLayout();
    }

    private void SetLayout()
    {
        GridLayoutGroup gridLayoutGroup = GetComponent<GridLayoutGroup>();
        RectTransform parentRect = parent.GetComponent<RectTransform>();

        gridLayoutGroup.cellSize = new Vector2(parentRect.rect.width / divideCols, height);
    }
}
