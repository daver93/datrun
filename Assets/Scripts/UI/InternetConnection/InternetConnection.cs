using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class InternetConnection : MonoBehaviour
{
    private Animator animator;

    public bool isConnected = false;

    void Start()
    {
        animator = GetComponent<Animator>();
        StartCoroutine(CheckInternetConnection());
    }

    public void OnConnectionTryAgain()
    {
        if (animator != null)
        {
            animator.SetTrigger("HideMessage");
            animator.SetTrigger("ShowMessage");
            StartCoroutine(CheckInternetConnection());
        }
    }

    IEnumerator CheckInternetConnection()
    {
        if (animator != null)
        {
            UnityWebRequest request = new UnityWebRequest("www.google.com");
            yield return request.SendWebRequest();

            if (request.error != null) // Did not achieve connection
            {
                isConnected = false;
                animator.SetTrigger("ShowMessage");
                Debug.Log($"Internet connection failed");
            }
            else // Connection achieved successfully
            {
                isConnected = true;
                animator.SetTrigger("HideMessage");
                Debug.Log($"Internet connection succeed");
            }
        }
    }
}
