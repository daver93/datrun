using UnityEngine;
using UnityEngine.SceneManagement;

public class Play : MonoBehaviour
{
    private GameObject internetConnection;

    private void Start()
    {
        internetConnection = GameObject.FindGameObjectWithTag("InternetConnection");
    }

    public void OnPressPlayAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void OnPressPlay()
    {
        SceneManager.LoadScene("Level_001_New");
    }

    public void OnPressPlayEndless()
    {
        SceneManager.LoadScene("EndlessRunner");
    }

    public void OnPressMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void OnPressScores()
    {
        SceneManager.LoadScene("LeaderBoard");
    }

    public void OnPressLeaderboard()
    {
        if (internetConnection.GetComponent<InternetConnection>().isConnected)
        {
            SceneManager.LoadScene("LeaderBoard");
        }
        else
        {
            internetConnection.SendMessage("OnConnectionTryAgain");
        }
    }
}
