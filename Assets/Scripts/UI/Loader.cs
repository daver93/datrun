using UnityEngine;

public class Loader : MonoBehaviour
{
    public void StopLoadingEventHandler()
    {
        gameObject.SetActive(false);
    }
}
