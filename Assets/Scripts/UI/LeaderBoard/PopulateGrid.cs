using LootLocker.Requests;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopulateGrid : MonoBehaviour
{
    public GameObject ScoreEntryPrefab;

    public void PopulateData(List<LootLockerLeaderboardMember> scores)
    {
        try
        {
            GameObject scoreObject;

            foreach (var scoreData in scores)
            {
                scoreObject = Instantiate(ScoreEntryPrefab, transform);

                for (int i = 0; i < scoreObject.transform.childCount; i++)
                {
                    Transform currentChild = scoreObject.transform.GetChild(i);

                    if (currentChild.name == "Username")
                    {
                        currentChild.gameObject.GetComponent<Text>().text = !string.IsNullOrWhiteSpace(scoreData.metadata) ? scoreData.metadata : scoreData.member_id.ToString();
                    }
                    else if (currentChild.name == "Score")
                    {
                        currentChild.gameObject.GetComponent<Text>().text = scoreData.score.ToString();
                    }
                    else if (currentChild.name == "Position")
                    {
                        currentChild.gameObject.GetComponent<Text>().text = scoreData.rank.ToString();
                    }
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }
}
