using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    #region Properties and Fields

    public float moveSpeed;
    public float leftRightSpeed = 4;
    public Animator playerAnimator;

    public bool HasVisibleRoadLimits;

    private Vector3 lastPosition;

    public int LastPosition
    {
        get { return (int)lastPosition.z; }
    }

    #endregion Properties and Fields

    private void Start()
    {
        playerAnimator.SetBool("Run", false);
        lastPosition = gameObject.transform.position;
    }

    private void Update()
    {
        if (LevelManager.Instance.IsPlaying)
        {
            ContinueRunning();
            SetCeilToIntLastPositionZ();
        }
        else
        {
            StopRunning();
        }
    }

    private void StopRunning()
    {
        playerAnimator.SetBool("Run", false);
    }

    private void ContinueRunning()
    {
        playerAnimator.SetBool("Run", true);

        transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed, Space.World);

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Moved)
            {
                if (touch.position.x > (Screen.width / 2) + 2)
                {
                    GoRight();
                }
                else if (touch.position.x < (Screen.width / 2) - 2)
                {
                    GoLeft();
                }
            }
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            GoRight();
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            GoLeft();
        }
    }

    private void GoRight()
    {
        if (gameObject.transform.position.x < LevelBoundary.rightSide || HasVisibleRoadLimits)
        {
            transform.Translate(Vector3.right * Time.deltaTime * leftRightSpeed);
        }
    }

    private void GoLeft()
    {
        if (gameObject.transform.position.x > LevelBoundary.leftSide || HasVisibleRoadLimits)
        {
            transform.Translate(Vector3.left * Time.deltaTime * leftRightSpeed);
        }
    }

    private void SetCeilToIntLastPositionZ()
    {
        var intPlayerPos = Mathf.CeilToInt(gameObject.transform.position.z);
        if (intPlayerPos > Mathf.CeilToInt(lastPosition.z))
        {
            lastPosition.z = intPlayerPos;
        }
    }
}
