using UnityEngine;

// Planks Collection should be attached on player
public class PlanksCollection : MonoBehaviour
{
    #region Properties and Fields

    [SerializeField]
    private int activePlanks;

    public GameObject planks;
    public GameObject bagPosition;

    public static int PlanksCounter { get; set; } = 0;
    public static PlanksCollection Instance;

    public int ActivePlanks
    { 
        get { return activePlanks; }
        set { activePlanks = value; }
    }

    #endregion Properties and Fields

    private void Awake()
    {
        Instance = this;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Plank")
        {
            PlanksCounter++;
            Destroy(other.gameObject);
            ShowPlankInCart(true);
        }
    }

    private void ShowPlankInCart(bool show)
    {
        if (show)
        {
            ActivePlanks++;
        }

        string nextPlankName = GetNameOfNextPlank(ActivePlanks);

        if (!show & ActivePlanks > 0)
        {
            ActivePlanks--;
        }

        bagPosition.transform.Find(nextPlankName).gameObject.SetActive(show);
    }

    private string GetNameOfNextPlank(int activePlanks)
    {
        string nextPositionNumberInitials = "";

        if (activePlanks <= 9)
        {
            nextPositionNumberInitials = "00";
        }
        else if (activePlanks <= 99)
        {
            nextPositionNumberInitials = "0";
        }

        return $"Position_{nextPositionNumberInitials}{ActivePlanks}";
    }

    // This should be called when planks got used to build a bridge
    public void HidePlankFromCart(int numberOfPlanksToHide)
    {
        for (int i = 0; i < numberOfPlanksToHide; i++)
        {
            ShowPlankInCart(false);
        }
    }
}
