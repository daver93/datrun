using System.Collections;
using UnityEngine;

// This class should be assigned on Player
public class Build : MonoBehaviour
{
    #region Properties and Fields

    [SerializeField]
    private bool canBuild;
    [SerializeField]
    private int requiredPlanksForBridge = 2;
    private Vector3 bridgePosition;
    private PlanksCollection planksCollection;
    private WaitForSecondsRealtime waitForSecondsRealtime;

    public float seconds;
    public GameObject bridge;

    public bool CanBuild
    {
        get { return canBuild; }
        set { canBuild = value; }
    }

    public int RequiredPlanksForBridge
    {
        get { return requiredPlanksForBridge; } 
        set { requiredPlanksForBridge = value; }
    }

    #endregion Properties and Fields

    private void Start()
    {
        CanBuild = true;
        RequiredPlanksForBridge = 2;
        seconds = 0.25f;

        planksCollection = PlanksCollection.Instance;
    }

    private void Update()
    {
        if (!LevelManager.Instance.IsPlaying)
        {
            return;
        }

        if (CanBuild && planksCollection.ActivePlanks >= 2)
        {
            //Check if the device running this is a handhel (mobile, tablet etc)
            if (SystemInfo.deviceType == DeviceType.Handheld)
            {
                if (Input.touchCount > 0)
                {
                    Touch touch = Input.GetTouch(0);

                    if (touch.tapCount == 2)
                    {
                        BuildBridge();
                    }
                }
            }
            //Check if the device running this is a desktop
            else if (SystemInfo.deviceType == DeviceType.Desktop)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    BuildBridge();
                }
            }
        }
    }

    private void BuildBridge()
    {
        for (int i = 0; i < RequiredPlanksForBridge; i += RequiredPlanksForBridge)
        {
            if (planksCollection.ActivePlanks >= 2)
            {
                planksCollection.HidePlankFromCart(RequiredPlanksForBridge);
            }
        }

        FindPositionForBridge();
    }

    private void FindPositionForBridge()
    {
        bridgePosition = this.gameObject.transform.Find("BridgePosition").position;
        bridge.transform.position = bridgePosition;

        Instantiate(bridge).SetActive(true); // Create a new bridge

        //StartCoroutine(DisableBuildingForSecons(seconds));
    }

    private IEnumerator DisableBuildingForSecons(float seconds)
    {
        CanBuild = false;

        if (waitForSecondsRealtime == null)
        {
            waitForSecondsRealtime = new WaitForSecondsRealtime(seconds);
        }
        else
        {
            waitForSecondsRealtime.waitTime = seconds;
        }

        yield return waitForSecondsRealtime;

        CanBuild = true;
    }
}
