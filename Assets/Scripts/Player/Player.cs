using UnityEngine;

public class Player : MonoBehaviour
{
    public string Username = null;

    public static Player Instance;

    private void Awake()
    {
        Instance = this;
    }
}
