using UnityEngine;

public class Score : MonoBehaviour
{
    #region Properties and Fields

    [SerializeField]
    private int totalScore;

    public static Score Instance;

    public int TotalScore
    {
        get { return totalScore; }
        set { totalScore = value; }
    }

    #endregion Properties and Fields

    private void Awake()
    {
        Instance = this;
    }

    public void CalculateScore(string finishedPlatformName)
    {
        switch (finishedPlatformName)
        {
            case "FinishLine_001":
                totalScore += 5;
                break;
            case "FinishLine_002":
                totalScore += 10;
                break;
            case "FinishLine_003":
                totalScore += 20;
                break;
            case "FinishLine_004":
                totalScore += 50;
                break;
            case "FinishLine_005":
                totalScore += 200;
                break;
        }
    }
}
