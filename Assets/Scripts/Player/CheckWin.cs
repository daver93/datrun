using System;
using UnityEngine;
using UnityEngine.UI;

public class CheckWin : MonoBehaviour
{
    private LevelManager levelManager;

    private void Start()
    {
        levelManager = LevelManager.Instance;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (levelManager.IsPlaying)
        {
            if (collision.gameObject.tag == "FinishLine")
            {
                FinishLevel(true, collision.gameObject.name);
            }
            else if (collision.gameObject.tag == "Obstacle")
            {
                FinishLevel(false);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (levelManager.IsPlaying)
        {
            if (other.gameObject.tag == "Background")
            {
                FinishLevel(false);
            }
        }
    }

    private void FinishLevel(bool isWin, string winPlatformName = null)
    {
        levelManager.IsPlaying = false;

        try
        {
            GameObject distanceObject = GameObject.FindGameObjectWithTag("DistanceText");
            Text distanceText = distanceObject.GetComponent<Text>();
            GameObject leaderBoardController = GameObject.Find("LeaderboardController");

            if (distanceText != null && leaderBoardController != null)
            {
                levelManager.ShowLostMessage();

                int.TryParse(distanceText.text, out int score);

                leaderBoardController.SendMessage("SubmitScore", score);
            }
            else
            {
                if (isWin)
                {
                    Score.Instance.CalculateScore(winPlatformName);
                    levelManager.ShowWinMessage();
                }
                else
                {
                    levelManager.ShowLostMessage();
                }
            }
        }
        catch (Exception e)
        {
            // Do nothing
        }
    }
}
