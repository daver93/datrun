using UnityEngine;

public class Background : MonoBehaviour
{
    private int lastPosition;
    private PlayerMove playerMove;

    private void Start()
    {
        playerMove = GameObject.Find("Player").GetComponent<PlayerMove>();
        lastPosition = playerMove.LastPosition;
    }

    private void Update()
    {
        // last position is used to check if there is any reason to move the background and it's relative to Player's position
        if (lastPosition != playerMove.LastPosition)
        {
            lastPosition = playerMove.LastPosition;
            MoveForward();
        }
    }

    /// <summary>
    /// Moves the background forward, so it will follow the player and it will be always visible
    /// This will create the illusion that the background is everywhere, but it will not
    /// With that way we will also have less geometery and thus memory to use
    /// </summary>
    private void MoveForward()
    {
        Vector3 increasePositionByZ = new Vector3(0, 0, 1);
        this.gameObject.transform.position += increasePositionByZ;
    }
}
