using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateLevel : MonoBehaviour
{
    public GameObject[] section;
    public float zPos = 50;
    public bool creatingSection = false;
    public int secNum;
    public int maxSecNum = 3;
    public float waitSeconds = 4;
    public int minPlatformOffset = 10;
    public int maxPlatformOffset = 20;

    public GameObject lastInstantiatedObject = null;

    private void Update()
    {
        if (!creatingSection)
        {
            creatingSection = true;
            StartCoroutine(GenerateSection());
        }
    }

    private IEnumerator GenerateSection()
    {
        secNum = Random.Range(0, maxSecNum);

        GameObject newInstance = Instantiate(section[secNum], new Vector3(0, 0, zPos), Quaternion.identity);

        int platformOffset = Random.Range(minPlatformOffset, maxPlatformOffset);
        
        zPos += (GetColliderSizeZ(newInstance) + platformOffset);

        lastInstantiatedObject = section[secNum];

        yield return new WaitForSeconds(waitSeconds);
        creatingSection = false;
    }

    public List<Vector3> sizeV;

    private float GetColliderSizeZ(GameObject gameObject)
    {
        var boundSize = gameObject.GetComponent<BoxCollider>().size;
        return boundSize.z;
    }
}
