using System.Collections;
using TMPro;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    #region Properties and Fields

    public bool isEndlessRunner;

    private bool canStart = false;
    private bool isPlaying = false;
    private WaitForSecondsRealtime waitForSecondsRealtime;

    public float secondsBeforeStart;
    public TextMeshProUGUI message;
    public static LevelManager Instance;

    public bool CanStart
    {
        get { return canStart; }
        set { canStart = value; } 
    }

    public bool IsPlaying
    {
        get { return isPlaying; }
        set { isPlaying = value; }
    }

    #endregion Properties and Fields

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        WaitBeforeStart(secondsBeforeStart);
        CanStart = true;
    }

    private IEnumerator WaitBeforeStart(float seconds)
    {
        if (waitForSecondsRealtime == null)
        {
            waitForSecondsRealtime = new WaitForSecondsRealtime(seconds);
        }
        else
        {
            waitForSecondsRealtime.waitTime = seconds;
        }

        yield return waitForSecondsRealtime;        
    }

    private void Update()
    {
        if (CanStart && !IsPlaying)
        {
            PressToStart();
        }
    }

    private void PressToStart()
    {
        if (!IsPlaying && CanStart && Input.anyKeyDown)
        {
            IsPlaying = true;
        }
    }

    public void ShowWinMessage()
    {
        message.text = $"You Win!! Score: {Score.Instance.TotalScore}";
        message.gameObject.SetActive(true);
        IsPlaying = false;
        CanStart = false;
    }

    public void ShowLostMessage()
    {
        message.text = $"You Lost :(";
        message.gameObject.SetActive(true);
        IsPlaying = false;
        CanStart = false;
    }
}
